const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const gravatar = require('gravatar');
const jwt = require('jsonwebtoken');
const passport = require('passport');

const keys = require('../config/keys');
const User = require('../models/User');

router.get('/test', (req, res) => {
    res.json({ msg: 'login page' })
})

router.post('/register', (req, res) => {
    console.log(req.body);
    // res.json(req.body)

    User.findOne({ email: req.body.email })
        .then((user) => {
            if (user) {
                return res.status(400).json({
                    email: '邮箱已被注册',
                    msg: '邮箱已被注册',
                    code: 0
                })
            } else {

                const avatar = gravatar.url(req.body.email, { s: '200', r: 'pg', d: 'mm' });

                const newUser = new User({
                    name: req.body.name,
                    email: req.body.email,
                    avatar,
                    password: req.body.password
                })

                bcrypt.genSalt(10, (err, salt) => {
                    bcrypt.hash(req.body.password, salt, (err, hash) => {
                        if (err) throw err;
                        newUser.password = hash;

                        newUser.save()
                            .then(user => res.json({
                                email: '注册成功',
                                msg: '注册成功',
                                code: 1
                            }))
                            .catch(err => console.log(err));
                    });
                });
            }
        })
})

router.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    User.findOne({ email })
        .then(user => {
            if (!user) {
                return res.status(404).json({
                    email: '用户不存在',
                    msg: '用户不存在',
                    code: 0
                })
            } else {
                bcrypt.compare(password, user.password)
                    .then(isMatch => {
                        if (isMatch) {
                            const rule = { id: user.id, name: user.name }
                            // console.log(rule);
                            jwt.sign(rule, keys.secretOrKey, { expiresIn: 3600 }, (err, token) => {
                                if (err) throw err;
                                res.json({
                                    token: 'Bearer ' + token,
                                    msg: '登录成功',
                                    code: 0
                                })
                            })
                        } else {
                            return res.status(400).json({
                                password: '密码错误',
                                msg: '密码错误',
                                code: 1
                            })
                        }
                    })
            }
        })
        .catch(err => console.log(err))
})


// 拿不到 ToKen
router.get('/current', passport.authenticate('jwt', { session: false }), (req, res) => {
    res.json({ msg: 'ok' })
    console.log(res);
})

module.exports = router;