const express = require('express');
const app = express();
const mongoose = require('mongoose');
const passport = require('passport');


const hostName = 'localhost';
const port = process.env.PORT || 8888;

const users = require('./routes/users');
const home = require('./routes/home');

const db = require('./config/keys');

app.use(express.urlencoded({ extended: false }));
app.use(express.json());

mongoose.connect(db.mongoURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => {
        console.log(`MongoDB Connected`);
    })
    .catch(err => {
        console.log(err);
    })

app.use(passport.initialize());
require('./config/passport')(passport);

app.use('/api/users', users);
app.use('/', home);


app.listen(port, hostName, (err) => {
    if (err) console.log(err);
    console.log(`Server running on http://${hostName}:${port}`);
})